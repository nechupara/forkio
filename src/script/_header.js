let menu = document.querySelector(".header-nav__menu")
let menu_button_pull = document.querySelector(".svg-menu--Rectangle_2-dims");
let menu_button_close = document.querySelector(".svg-menu--Vector_3-dims");

document.addEventListener("click", (event) => {
     
    if ( event.target.classList.contains("svg-menu--Rectangle_2-dims"))
        {        
        menu.style.display = "flex";        
        menu_button_pull.style.display = "none";
        }
    else if(event.target.classList.contains("header-nav__links") && (menu_button_pull.style.display == "none"))
        {
        menu.style.display = "flex";       
        menu_button_pull.style.display = "none";
        }
    else{
        menu.removeAttribute("style");      
        menu_button_pull.removeAttribute("style");
        }
    }
 
)
